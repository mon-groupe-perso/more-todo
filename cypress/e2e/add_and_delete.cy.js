describe("Todos test", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000");
  });
  it("USER CAN ADD, CHECK AND DELETE TODOS", () => {
    cy.contains("Click here to login").click();
    cy.get("#title").type("Aller au supermarché").type("{enter}");
    cy.get("#title").type("Aller chez le dentiste").type("{enter}");
    cy.get("#container").should("contain.text", "Aller au supermarché");
    cy.get("#container").should("contain.text", "Aller chez le dentiste");
    cy.get("#container").should("contain.text", "Total Todos: 2");
    cy.get('[type="checkbox"]').check();
    cy.get("#container").should("contain.text", "Selected Todos: 2");
  });
});
